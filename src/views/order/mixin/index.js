export default {
    date() {
        return {

        }
    },
    methods: {
        //type 1 列表操作  2 详情操作
        order_close( item ,index ) {
            let id = item.id
    
            this.$dialog.confirm({ title: '提示', message: '确定取消该订单吗' }).then(res => {
                this.m_loading()
                this.api.order_close({ id }).then(res => {
                    this.m_hideLoading()
                    this.m_toast('关闭成功')
                    this.order_close_cb && this.order_close_cb(item ,index )

                })
            }).catch(err => {

            })
        },

        goPay(data){
            this.$router.push({path:'/goodsPay',query:{order_id:data.id,order_no:data.order_no,amount:data.goods_amount,type:data.type}})
        }

    }
}