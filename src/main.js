import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
Vue.config.productionTip = false
import './assets/js/flexible'
import './assets/scss/base.scss'
import './assets/scss/public.scss'
import './assets/scss/y.scss'
import plugin from '@/plugins/index.js'
import VConsole from 'vconsole'
import i18n from './utils/i18n'
import config from './config/index'
// import FastClick from 'fastclick'
// FastClick.attach(document.body);
require('@/assets/js/BridgeSDK-1.0.0')

const ENV = process.env.VUE_APP_ENV
Vue.use(plugin, {})
if (ENV == 'production') {
 // new VConsole()
}

if (ENV != 'development') {

}

const windowHeight = window.innerHeight
Vue.directive('fixedInput', function (el, binding) {
  el.addEventListener('blur', function () {
    let windowFocusHeight = window.innerHeight
    if (windowHeight == windowFocusHeight) {
      return
    }
    let currentPosition;
    let speed = 1; //页面滚动距离
    currentPosition = document.documentElement.scrollTop || document.body.scrollTop;
    currentPosition -= speed;
    window.scrollTo(0, currentPosition); //页面向上滚动
    currentPosition += speed; //speed变量
    window.scrollTo(0, currentPosition); //页面向下滚动
  })
})


import Es6Promise from 'es6-promise'
Es6Promise.polyfill()
import './router/interception' //路由拦截器

import {
  Button, NavBar, Icon, Tabbar, TabbarItem, Field, Cell, CellGroup, Toast, Checkbox, CheckboxGroup, Dialog, Popup, Tab, Tabs, Panel, List, Picker, Card, Area, Stepper, Swipe, SwipeItem, Loading, RadioGroup, Radio, Tag, Search, PullRefresh, Sidebar, SidebarItem, ActionSheet, DropdownMenu, DropdownItem, Image, Grid, GridItem, CountDown, Divider, Lazyload, GoodsAction, GoodsActionIcon ,
  GoodsActionButton ,SubmitBar,ContactCard, ContactList, ContactEdit ,Step, Steps ,Rate,Uploader,Sticky
} from 'vant';
import VueClipboard from 'vue-clipboard2'

Vue.use(Button).use(NavBar).use(Icon).use(Tabbar).use(TabbarItem).use(Field).use(Cell).use(CellGroup).use(Toast).use(Checkbox).use(CheckboxGroup).use(Dialog).use(Popup).use(Tab).use(Tabs).use(Panel).use(List).use(Picker).use(Card).use(Area).use(Stepper).use(Swipe).use(SwipeItem).use(Loading).use(RadioGroup).use(Radio).use(Tag).use(Search).use(PullRefresh).use(Sidebar).use(SidebarItem).use(ActionSheet).use(DropdownMenu).use(DropdownItem).use(Image).use(Grid).use(GridItem).use(GridItem).use(CountDown).use(Divider).use(Lazyload).use(GoodsAction)
.use(GoodsActionIcon).use(GoodsActionButton).use(SubmitBar).use(ContactCard).use(Sticky)
.use(ContactList).use(Rate).use(Uploader)
.use(ContactEdit).use(Step).use(Steps).use(VueClipboard)
window.vm = new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')


