export default {

    index: '主页',
    mall: '商城',
    index_my: '个人中心',
    my_pinpai: '品牌券明细',
    pinpaiExchange: '商城券转换',
    name: '姓名',
    mobile: '手机号',
    login: '登录',
    register: '注册',
    findPwd: '找回密码',
    sign_out: '退出',


    qrcode: '邀请好友',
   
    order: '订单管理',
    pay_credit: '收益明细',
    orderDetail: '订单详情',


    goodsInfo: '商品详情',
 
    goodsOrder: '商品订单',
    address: '收货地址',
    balance: '财务报表',


    myinfo: '个人资料',
    myMsg: '消息列表',
    msgDetails: '消息详情',
    setLang: '设置语言',

    cancel: '取消',
    Balance_pay: '余额支付',

    regain: '重新获取',
    auth_code: '获取验证码',
    hour: '时',
    minute: '分',
    second: '秒',
    noData: '没有数据',


    tips: '提示',


    l_mobile: '手机',
    l_mobile_tip: '请输入手机号码',
    l_mobile_error_tip: '输入正确的手机号码',











    record_no: '流水号',
    type: '类型',
    amount: '金额',
    created_at: '时间',
    remark: '备注',






    order_no: '订单号',
    order_status: '订单类型',
    Income_details: '收益明细',
    Close: '关闭',
    status: '状态',
    d_endTime: '倒计时',
    expire_at: '截止时间',
    goods: '商品',
    look: '查看',
    remark: '备注',


    num: '数量',
    total_money: '总金额',









    price: '价格',

    goodsInfo_tip7: '个',
    mall_tip1: '未开放',
    mall_tip2: '该产品未开放,敬请期待！',



    buyFruitsRecord:'购买记录',
    
    status_1:'审核失败',
    status1:'审核中',
    status4:'已完成',

    sys_info:'系统公告',







    set_success:'设置成功',

    auto_code:'短信验证码',


    copy:'复制',
    paste:'粘贴',

    paste_Success:'复制成功',
    no_set:'未设置',
    has_set:'已设置',


  

    pay_type:'支付类型',

    about: '关于我们',
    about_us: '公司简介',
    contact: '联系我们',
    
    login_ing: '登陆',
    load_ing: '加载中',
    upload_ing: '上传中',
    user_register: '用户注册',

}