export default {
    index: '主頁',
    mall: '商城',
    index_my: '個人中心',
    name: '姓名',
    mobile: '手機號',
    login: '登錄',
    register: '註冊',
    findPwd: '找回密碼',
    sign_out: '退出',
    updataPwd: '修改密碼',
    customerService: '專屬客服',
    qrcode: '邀請好友',
    mytear: '團隊管理',
    order: '訂單管理',
    pay_credit: '收益明細',
    orderDetail: '訂單詳情',
    payDetailt: '訂單詳情',
    menu_mall: '升級商場',
    buyFruits: '果子種子',
    goodsInfo: '商品詳情',
    voucherUpload: '上傳憑證',
    goodsOrder: '商品訂單',
    address: '收獲地址',
    balance: '財務報表',
    auto: '實名認證',
    bandBank: '綁定銀行卡',
    myinfo: '個人資料',
    withdraw: '提現申請',
    myMsg: '消息列表',
    msgDetails: '消息詳情',
    setLang: '設置語言',

    cancel: '取消',
    Balance_pay: '余額支付',
    wx_pay: '微信支付',
    regain: '重新獲取',
    auth_code: '獲取驗證碼',
    hour: '時',
    minute: '分',
    second: '秒',
    noData: '沒有數據',

    static_jj: '靜態獎金',
    dynamic_jj: '動態獎金',
    credit1_jj: '賬戶余額',
    credit2_num: '種子數量',
    credit3_num: '果子數量',
    credit4_num: '爵士數量',

    tips: '提示',
    Complete_name_auto: '請先完成實名認證！',
    user_register: '用戶註冊',
    l_mobile: '手機',
    l_mobile_tip: '請輸入手機號碼',
    l_mobile_error_tip: '輸入正確的手機號碼',

    l_sure_pwd: '確認密碼',
    l_pwd: '密碼',
    l_pwd_tip: '請輸入密碼',
    set_pwd_tip: '請設置登錄密碼',
    find_pwd: '找回登錄密碼',
    p_name_pwd: '請輸入賬號密碼',

    t_people: '推薦人（必填）',
    t_people_tip: '請輸入推薦人手機號碼',
    get_auto_code: '獲取手機驗證碼',
    upload_app: '已有賬號?下載app',
    input_auto_code: '輸入短信驗證碼',
    input_auto_code_tip: '請輸入驗證碼',

    submit: '提交',
    submitSuccess: '提交成功',
    back: '返回',
    res_success: '註冊成功',
    login_ing: '登錄中..',
    sendSuccess: '發送成功',

    updata_old_pwd: '舊密碼',
    updata_old_pwd_tip: '請輸入舊密碼',
    updata_new_pwd: '新密碼',
    updata_new_pwd_tip: '請輸入新密碼',
    re_pwd: '重復密碼',
    no_eq_pwd: '2次密碼不壹致',
    no_6_pwd: '密碼要大於6位',
    sumbit_ing: '提交中..',
    updata_success: '修改成功',

    keep_card_image: '手持身份證正面',
    keep_card_image_tip: '手持身份證正面示例',
    front_image: '身份證正面',
    front_image_tip: '身份證正面示例',
    back_image: '身份證反面',
    back_image_tip: '身份證反面示例',
    realname: '真實姓名',
    realname_tip: '請輸入真實姓名',
    idCard: '身份證號',
    idCard_tip: '請輸入身份證號',
    upload_ing: '上傳中..',
    uploadSuccess: '上傳成功',
    uploadErr: '上傳失敗',
    error_tip: '失敗提示',
    bank_front_image: '銀行卡正面',
    bank_front_image_tip: '銀行卡正面示例',
    bank_back_image: '銀行卡反面',
    bank_back_image_tip: '銀行卡反面示例',


    record_no: '流水號',
    type: '類型',
    amount: '金額',
    created_at: '時間',
    remark: '備註',

    bank: '銀行',
    bank_tip: '選擇妳的開戶銀行',
    bank_no: '卡號',
    bank_no_tip: '請輸入卡號',
    bank_holder: '戶名/持卡人',
    bank_holder_tip: '請輸入戶名/持卡人',
    bank_mobile: '預留手機號',
    bank_mobile_tip: '請輸入預留手機號',
    created_place: '開戶行',
    created_place_tip: '請輸入開戶行',

    Tel_consultation: '電話咨詢',
    wx_consultation: '微信咨詢',
    ser_tip1: '基於JS全球的發展戰略，將逐步完善全產業生態圈層，分階段推動“5D全息投影”構想，利用區塊鏈技術，建設生態激勵體系，與用戶及服務商共享平臺增值，JS最終打造壹個全球化人工智能化的視覺應用新平臺！',
    ser_tip2: '妳在平臺有任何問題，可咨詢本商城客服。',
    ser_tip3: '平臺專屬服務經理',
    ser_tip4: '我知道了',

    search_tip: '請輸入賬號/姓名',
    total_peo: '總人數',
    team_name: '賬號',
    team_level: '等級',
    team_child_level: '子級',

    order_no: '訂單號',
    order_status: '訂單類型',
    Income_details: '收益明細',
    Close: '關閉',
    status: '狀態',
    d_endTime: '倒計時',
    expire_at: '截止時間',
    goods: '商品',
    look: '查看',
    remark: '備註',
    remark_close_tip: '填寫關閉原因',

    num: '數量',
    total_money: '總金額',
    pay_credit_status: '憑證狀態',

    da_money: '打款金額',
    pay_detail_tip1: '請在24小時向以下賬號打入以上金額',
    pay_detail_tip2: '務必核對賬號賬號，打款後請上傳打款憑證圖片',
    nono: '已經失效',
    bank_holder_1: '收款人（持卡人）',

    emit_name: '打款人',
    remit_phone: '打款人電話',
    da_sure: '打款確認',
    se_Err: '審核失敗',
    se_Success: '審核成功',

    can_money: '可提現金額',
    can_money_tip: '請輸入提現金額',
    withdraw_tip1: '請輸入金額',
    withdraw_tip2: '請輸入大於0的金額',
    withdraw_tip3: '申請金額大於可提現金額',

    fruits_tip1: '是購買產品的條件',
    fruits_tip2: '總計金額',
    fruits_tip3: '折合',
    fruits_tip4: '平臺轉賬',
    fruits_tip5: '上傳轉賬憑證',
    fruits_tip6: '是否刪除此轉賬憑證',
    fruits_tip7: '提交成功，我們將會盡快處理！',

    price: '價格',
    goodsInfo_tip1: '種子',
    goodsInfo_tip2: '果子',
    goodsInfo_tip3: '立即購買',
    goodsInfo_tip4: '操作成功,等待系統排隊！',
    goodsInfo_tip5: '將花費種子',
    goodsInfo_tip6: '購買商品,是否確認購買？',
    goodsInfo_tip7: '個',

    mall_tip1: '未開放',
    mall_tip2: '該產品未開放,敬請期待！',

    voucherUpload_tip1: '添加轉賬憑證',
    voucherUpload_tip2: '是否刪除此轉賬憑證',

    buyFruitsRecord:'購買記錄',

    status_1:'審核失敗',
    status1:'審核中',
    status4:'已完成',

    sys_info:'系統公告',

    checkUpdata:'檢查更新',

    h5_version_1:'當前版本為',
    h5_version_2:'最新版本為',
    h5_version_3:'是否確定更新',

    tranPwd:'交易密碼',
    set_success:'設置成功',
    tranPwd_tip:'設置交易密碼',
    tranPwd_tip1:'重置交易密碼',
    auto_code:'短信驗證碼',

    setPayUrl:'提幣地址',
    copy:'復制 ',
    paste:'粘貼',
    packUrl:'錢包地址',
    paste_Success:'復制成功',

    no_set:'未設置',
    has_set:'已設置',

    no_set_tranPwd:'未設置交易密碼',
    no_set_tranPwd_tip1:'是否前往設置交易密碼？',
    pay_type:'支付類型',
    
    fruitsDetailList:'果子明細',
    coinDetail:'JS幣明細'  ,
    fee:'手續費' ,
    fruitsToCoin:'果子兌換JS幣',
    fruitsToCoin_tip1:'兌換JS幣',
    
    fruitsToCoin_tip2:'果子最多可換',

    fruitsToCoin_tip3:'請輸入兌換的果子數量',
    exchange:'兌換',
    exchange_success:'兌換成功',



    js_withdraw:"JS幣提現",
    js_withdraw_tip1:'提現金額為',















}