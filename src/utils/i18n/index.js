import Vue from 'vue'
import VueI18n from 'vue-i18n'

import { Collapse, CollapseItem } from 'vant';

Vue.use(Collapse).use(CollapseItem);
Vue.use(VueI18n)


import zh_TW from './tw/index'
import zh_CN from './zh/index'



const messages = {
    'zh': zh_CN, // 中文语言包
    'tw': zh_TW, // 繁体语言包

}


let lang = localStorage.lang || 'zh'
export default new VueI18n({
    locale: 'zh', // set locale 默认显示
    messages: messages // set locale messages
})