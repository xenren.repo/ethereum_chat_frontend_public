/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj (url) {
    url = url || window.location.search
  
    const search = url.split('?')[1]
    if (!search) {
      return {}
    }
    return JSON.parse(
      '{"' +
        decodeURIComponent(search)
          .replace(/"/g, '\\"')
          .replace(/&/g, '","')
          .replace(/=/g, '":"')
          .replace(/\+/g, ' ') +
        '"}'
    )
  }


// obj1 的值覆盖到obj上 , （编辑表单时候用）
export const coverObj = (obj, obj1) => {
  let o = JSON.parse(JSON.stringify(obj))
  let o_key = Object.keys(o)
  o_key.forEach(el=>{
      o[el] = obj1[el] || o[el]
  })
  return o
}

export const getBaseUrl = () => {
  const location = window.location
   return location.origin + location.pathname + '#'
}