import store from '@/store'
import router from '@/router'
import * as api from '@/api'
import { param2Obj } from './index'
export const initData = (cb) => {
  return new Promise((resolve, reject) => {
    api.data_dictionary().then(res => {
      sessionStorage.baseData = JSON.stringify(res.data)
      api.site_info().then(res => {
        sessionStorage.site_info = JSON.stringify(res.data)
      }).then(res => {
        cb && cb()
        resolve(res)
      })
    }).catch(err=>{
      reject(res)
    })

  })



}

export const handelLogin = () => {
  let query = param2Obj()
  let data = {
    user_id: query.userId,
    token: query.token
  }
  return api.login_by_remote_token(data).then(res => {
    return res
  })

}


