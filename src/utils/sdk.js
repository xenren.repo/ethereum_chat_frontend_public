
export function getSdkEvn() {
    return window.android ||  ''
}

//支付宝 支付
export const aliPayInfo = (info,cb) => {
    //  let  Env = getSdkEvn()
    //   console.log(info,'info')
    //  window.alipayResult = cb
    BridgeSDK.webToAppAlipay(info,cb)
 }

export const wechatInfo = (info,cb) => {
    //  let  Env = getSdkEvn()
    //   console.log(info,'info')
    //  window.alipayResult = cb
    BridgeSDK.webToAppWechatPay(info,cb)
 }



 export const goToWebPage = (url) => {
    var options = {
        "url": url,       // 必填,页面地址
        //"title": "",                   // 可选,自定义标题,不传的话取网页的title
        // "type": "",                          // 原生界面标志
         "pageTag": "PageCustomerTag",         // 可选,页面唯一标志，方便运营统计分析
        // "navigation": {
        //     // "isHideNavBar": false,            // 是否隐藏导航栏，默认false
        //     // "isHideNavBarDividerLine": false, // 是否隐藏导航栏下的分割线，默认false
        //     "title": "",                   // 标题, 优先级最高
        //     "titleColor": "",      // 标题颜色,默认白色
        //     "titleFontSize": "",            // 标题字体大小
        //     "navbarBackgroundColor": "", // 导航栏背景颜色
        //     "rightButtonType": "",           // 1显示刷新(默认) 2自定义按钮
        //     "rightButtonTitle": "",    // 按钮标题
        //     "rightButtonTitleColor": "", // 标题文字颜色
        //     "rightButtonAction": "", // 按钮触发的js事件名称
        // }


    }
    BridgeSDK.goToWebPage(options)
}

//token 过期
export const invalidUserToken = ()=>{
    BridgeSDK.invalidUserToken({})
}

export const logout = (cb)=>{
    BridgeSDK.call('user_logout', '', cb)
}

//保存到相册
export const saveImage = (url, cb) => {
    BridgeSDK.saveImage({
        data: url,
        cb
    });
}