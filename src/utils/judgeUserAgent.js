import * as api from '@/api/index'
import _ from 'lodash'
import {initData} from './loginInitData'
import config from '../config/index'

export function isApp() {
    if (window.wx && typeof window.wx.getUserNameFromApp === 'function') {
        return true
    } else {
        return false
    }
}


export function isDingDing() {
    let ua = navigator.userAgent
    if (ua.search('DingTalk') !== -1) {
        return true
    } else {
        return false
    }
}

export function isWeixn() {
    let ua = navigator.userAgent
    if (ua.search('MicroMessenger') !== -1 && ua.search('wxwork') === -1) {
        return true
    } else {
        return false
    }
}

export function angent() {
    if (isWeixn()) { return 'weixin' }
    if (isDingDing()) { return 'dingding' }
    if (isApp()) { return 'wxapp' }
    return 'web'
}


export function judgeOption(to,from,next) { 
    let ANGENT = sessionStorage.ANGENT || ''
    if(!ANGENT){ return }
    switch (ANGENT){
      case 'web' :{
      
       web_is_login(to,from,next)
       
       return false
       
      }
      case 'weixin':{
       console.log('weixin')
       if(process.env.NODE_ENV=="production"){
        web_is_login(to,from,next)
           return  false
       }
       weixin_is_login(to,from,next)
      
       return false
      }
      case 'dingding':{
          console.log('dingding')
          dingding_is_login(to,from,next)
          return false

      }
      default : {
        web_is_login(to,from,next)    
        return false

      }

    }

 }

 //web 端 检查是否登陆
 function web_is_login(to,from,next){
 
    api.is_login().then(res=>{
        console.log(res)
      if(res.data.is_login){
          addData(next)
      }else{
        next('/index')
      }
    }).catch(res=>{ 
       next('/index')
    })
  }

   //微信认证 
   function weixin_is_login(to,from,next){
       next()
       return 
    let query = parseQuery(location.search.slice(1))
     
    if(!query.code){
        //地址要改
     api.site_info().then(res=>{
        //  localStorage.logo = res.data.logo
         localStorage.site_info = (JSON.stringify(res.data))
         window.location.href=`https://open.weixin.qq.com/connect/oauth2/authorize?appid=${res.data.app_id}&redirect_uri=${encodeURIComponent(window.location.origin + '/mobile.html')}&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect`

     }) 
    //  window.location.href=`https://open.weixin.qq.com/connect/oauth2/authorize?appid=${config[process.env.NODE_ENV].weixin}&redirect_uri=${encodeURIComponent(window.location.origin + '/mobile.html')}&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect`
    
    }else{
    //     api.wechat_code(query.code).then(res=>{
    //         sessionStorage.isGo = 1
    //         if(res.data.is_login!=1){        
    //             next()
    //         }else{
             
    //           addData(next)
             
    //         }
    //     }).catch(res=>{
    //       next()
    //     })
    //   console.log(query.code)
    }
   
   
}

  //钉钉认证
  function dingding_is_login(to,from,next){
     // alert('dingdingd')
      dd.ready(()=>{
        dd.runtime.permission.requestAuthCode({
            corpId: config[process.env.NODE_ENV].dingding,
            onSuccess: function(result) {  

            // api.ding_talk(result.code).then(res=>{
            //     if(!res.data.is_login){      
            //         next()     
            //     }else{
            //       addData(next)
            //     }
    
            // }).catch(res=>{
            //     next()
            // })
            },
            onFail : function(err) {
                console.log(err)
                next()
            }
         
        })

      })
    

  }

    //解析 url  query
    var parseQuery = function(query){
        var reg = /([^=&\s]+)[=\s]*([^=&\s]*)/g;
        var obj = {};
        while(reg.exec(query)){
            obj[RegExp.$1] = RegExp.$2;
        }
        return obj;
    }


 //登陆后加载数据
 function addData(next) {
    initData(()=>{
        next('/index')
    })
 }