//{params:data}
import axios from 'axios'
//登录
export const login = (data) => {
    return axios.post('/user/login', data)
}

export const autoLogin = (data) => {
    return axios.post('/user/auto-login', data)
}

//自动登录
export const is_login = () => {
    let _token = localStorage.token
    let _expire = localStorage.expire || 0
    if (!_token || !_expire) {
        return Promise.resolve({
            data: {
                is_login: 0
            }
        })
    } else {
        let _nowTime = new Date().getTime()
        _expire = parseInt(_expire)
        if (_nowTime - _expire >= 0) {
            return Promise.resolve({
                data: {
                    is_login: 0
                }
            })
        } else {
            return Promise.resolve({
                data: {
                    is_login: 1
                }
            })
        }
    }
}

//站点基础信息
export const site_info = (data) => {
    return axios.get('/site/info', data)
}

//注册
export const register = (data) => {
    return axios.post('/user/register', data)
}

//获取验证码
export const getSms = (data) => {
    return axios.post('/get_sms', data)
}

//数据字典
export const data_dictionary = (data) => {
    return axios.get('/data_dictionary', {
        params: {
            module: '',
            key_name: ''
        }
    })
}


export const get_settings = (data) => {
    return axios.get('/get_settings')
}

//注销
export const logout = () => {
    return axios.get('/user/logout')
}

//修改密码
export const change_password = (data) => {
    return axios.post(`/user/change_password`, data)
}



//密码重置
export const resetPwd = (data) => {
    return axios.post(`/user/reset_password`, data)
}

//获取用户信息
export const getUserInfo = () => {
    return axios.get('user/info')
}





//邀请好友
export const qrcode = () => {
    return axios.get('user/qrcode')
}

//系统公告列表 
export const sys_info = (data) => {
    return axios.get('/user/sys_info', { params: data })
}

//系统公告详情 
export const sys_info_show = (data) => {
    return axios.get('/user/sys_info/show', { params: data })
}



//余额明细
export const account_records = (data) => {
    return axios.get('/user/finance/credit1', { params: data })
}

//我的团队
export const team = (data) => {
    return axios.get('/user/team', { params: data })
}

//首页轮播图获取
export const ad = (data) => {
    return axios.get('/user/ad', data)
}


//个人收货地址列表 
export const address = (data) => {
    return axios.get('/user/address/list', { params: data })
}

//收件地址删除
export const address_del = (data) => {
    return axios.post('/user/address/delete', data)
}

//收件地址编辑
export const address_update = (data) => {
    return axios.post('/user/address/update', data)
}

//收件地址添加
export const address_add = (data) => {
    return axios.post('/user/address/add', data)
}





//订单明细对接
export const orders = (data) => {
    return axios.get('/user/orders', { params: data })
}



//订单详情
export const order_show = (data) => {
    return axios.get('/user/order/show', { params: data })
}

//关闭订单
export const shutdown = (data) => {
    return axios.post('/user/order/shutdown', data)
}




//分类产品搜索
export const category = (data) => {
    return axios.get('/user/goods/category', { params: data })
}

//分类产品搜索
export const today_recommend = (data) => {
    return axios.get('/user/goods/today_recommend', { params: data })
}

//用户签到
export const user_sign = (data) => {
    return axios.post('/user_sign/sign', data)
}







//广告获取
export const collected_credit_ad = (data) => {
    return axios.get('/user/collected_credit/ad', { params: data })
}


//广告获取
export const third_ad = (data) => {
    return axios.get('/user/third_ad/fetch', { params: data })
}









//用户基础信息设置
export const info_set = (data) => {
    return axios.post('/user/info/set', data)
}






/////////////////
//商城首页基础信息
export const mall_info = (data) => {
    return axios.get('/user/mall/info', { params: data })
}

//分类列表
export const goods_category = (data) => {
    return axios.get('/user/goods_category/index', { params: data })
}

//分类列表
export const goods_search = (data) => {
    return axios.get('/user/goods/search', { params: data })
}

//产品详情
export const goods_detail = (data) => {
    return axios.get('/user/goods/detail', { params: data })
}

//产品详情
export const goods_price = (data) => {
    return axios.get('/user/goods/current-price', { params: data })
}

//产品详情
export const goods_cart = (data) => {
    return axios.get('/user/goods_cart', { params: data })
}

//添加产品到购物车
export const goods_cart_add = (data) => {
    return axios.post('/user/goods_cart/add', data)
}

//编辑购物车购物车产品
export const goods_cart_update = (data) => {
    return axios.post('/user/goods_cart/update', data)
}

//购物车产品移除
export const goods_cart_delete = (data) => {
    return axios.post('/user/goods_cart/delete', data)
}

//默认收货地址获取
export const default_address = (data) => {
    return axios.post('/user/address/default_address', data)
}

//清空购物车
export const goods_cart_flush = (data) => {
    return axios.post('/user/goods_cart/flush', data)
}

//用户下单
export const order_add = (data) => {
    return axios.post('/user/order/add', data)
}

//
export const receipt_add = (data) => {
    return axios.post('/user/receipt/add', data)
}

//允许使用的支付方式
export const allow_list = (data) => {
    return axios.post('/user/pay/allow_list', data)
}

//支付
export const pay_order = (data) => {
    return axios.post('/user/pay/order', data)
}



//订单详情
export const order_detail = (data) => {
    return axios.get('/user/order/detail', { params: data })
}

//关闭订单
export const order_close = (data) => {
    return axios.post('/user/order/close', data)
}

//确认收货
export const order_confirm = (data) => {
    return axios.post('/user/order/confirm', data)
}


//物流信息查询
export const order_package_detail = (data) => {
    return axios.post('/user/order_package/detail', data)
}

export const order_package_trace = (data) => {
    return axios.post('/user/order_package/trace', data)
}

//评价
export const order_comment_comment = (data) => {
    return axios.post('/user/order_comment/comment', data)
}

//申请退款
export const refund = (data) => {
    return axios.post('/user/order/refund', data)
}

//产品评价查看
export const order_comment_list = (data) => {
    return axios.post('/user/order_comment/detail', data)
}

//维权进度查询
export const refund_detail = (data) => {
    return axios.post('/user/order/refund/detail', data)
}

//取消维权申请
export const cancel_refund = (data) => {
    return axios.post('/user/order/cancel_refund', data)
}

//用户ID&token登入
export const login_by_remote_token = (data) => {
    return axios.post('/user/login_by_remote_token', data)
}

//专区券明细
export const finance_credit2 = (data) => {
    return axios.get('/user/finance/credit2', { params: data })
}

//专区券充值申请
export const recharge_add = (data) => {
    return axios.post('/user/recharge/add', data)
}

//专区券明细
export const recharge = (data) => {
    return axios.get('/user/recharge', { params: data })
}

//品牌券明细
export const finance_credit3 = (data) => {
    return axios.get('/user/finance/credit3', { params: data })
}

//专区券充值取消
export const recharge_cancel = (data) => {
    return axios.post('/user/recharge/cancel', data)
}

//预下单信息计算
export const pre_count = (data) => {
    return axios.post('/user/order/pre_count', data)
}



//关于我们
export const about_us = (data) => {
    return axios.post('/user/about_us', data)
}

export const load_credit3 = (data) => {
    return axios.post('/user/finance/credit3', data)
}

export const load_credit4 = (data) => {
    return axios.post('/user/finance/credit4', data)
}

export const pinpai_exchange = (data) => {
    return axios.post('/user/exchange', data)
}

export const zone_coupon_exchange = (data) => {
    return axios.post('/user/finance/credit3/convert-to-zone-coupon', data)
}


// 关于我们
export const medium = (data) => {
    return axios.post('/user/medium', data)
}
// 关于我们详情
export const medium_show = (data) => {
    return axios.post('/user/medium/show', data)
}

// 关于我们详情
export const report_seller = (data) => {
    return axios.post('/user/report-seller', data)
}