(function (global, factory) {
    if (typeof require === 'function' && typeof module === "object" && module && module["exports"]) {
        module['exports'] = (function () {
            return factory();
        })();
    }
    else if (typeof define === 'function' && define.amd) {
        define([], factory);
    }
    else {
        global["BridgeSDK"] = factory();
    }
})(window, function () {

    "use strict";

    var u = navigator.userAgent;
    var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android终端
    var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
    var eventHub = new Map();
    var callBackList = new Map();
    var defaultParam = {
        action: "",
        data: "",
        appid: "",
        authtoken: ""
    };

    function __log__(content) {
        if (BridgeSDK && BridgeSDK.debug) {
            if (content && content.length > 0) {
                console.log(content)
            }
        }
    }

    /*建立桥接*/
    function setupWebViewJavascriptBridge(callback) {
        if (isAndroid) {
            try {
                if (window.WebViewJavascriptBridge) {
                    callback(WebViewJavascriptBridge);
                } else {
                    document.addEventListener("WebViewJavascriptBridgeReady", function () {
                        callback(WebViewJavascriptBridge);
                    }, false);
                }
            } catch (ex) {
                __log__(ex.toString())
            }
        } else if (isiOS) {
            if (window.WebViewJavascriptBridge) {
                return callback(WebViewJavascriptBridge);
            }
            if (window.WVJBCallbacks) {
                return window.WVJBCallbacks.push(callback);
            }
            window.WVJBCallbacks = [callback];
            var WVJBIframe = document.createElement('iframe');
            WVJBIframe.style.display = 'none';
            WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__';
            document.documentElement.appendChild(WVJBIframe);
            setTimeout(function () {
                document.documentElement.removeChild(WVJBIframe);
            }, 0);
        } else {
            __log__("not support")
            callback(undefined);
        }
    }

    /*初始化网页端桥接坏境*/
    setupWebViewJavascriptBridge(function (bridge) {
        if (typeof bridge === "undefined") {
            return;
        }

        if (isAndroid) {
            bridge.init(function (message, responseCallback) {
                // __log__(`init success:${message}`)
            });
        }

        // 注册给App调用
        bridge.registerHandler('jsHandler', function (data, responseCallback) {
            if (!data) {
                return;
            }
            var requestBody = JSON.parse(data);
            __log__(requestBody);

            var action = requestBody.action;
            var respData = requestBody.data;

            // 注册协议
            if (eventHub.has(action)) {
                eventHub.get(action)(respData, responseCallback);
            } else {
                if (action === 'PageStatusChange') {
                    var pageStatusChangeCallBack = callBackList.get("onPageStatusChange");
                    pageStatusChangeCallBack && pageStatusChangeCallBack(respData)
                }
                else if (action == 'NativeButtonClick') {
                    if (respData.type && respData.type == 1) {
                        var onRightButtonClick = callBackList.get('onRightBtnClick')
                        onRightButtonClick && onRightButtonClick(respData.data || '')
                    }
                }
                else if (action == 'onPageCallBack') {
                    var callBackFunc = callBackList.get("onPageCallBack");
                    callBackFunc && callBackFunc(respData)
                }
                else {
                    __log__('!!!无法处理协议:' + action)
                }
            }
        });
    });

    /**
     * 对象简单继承，后面的覆盖前面的，继承深度：deep=1
     * @private
     */
    var _extend = function () {
        var result = {}, obj, k;
        for (var i = 0, len = arguments.length; i < len; i++) {
            obj = arguments[i];
            if (typeof obj === 'object') {
                for (k in obj) {
                    if (typeof obj[k] == 'boolean') {
                        result[k] = obj[k]
                    } else {
                        obj[k] && (result[k] = obj[k])
                    }
                }
            }
        }
        return result;
    };

    // 全局对象
    var BridgeSDK = {
        version: "1.0.0",
        debug: false,
        authconifg: {
            "appid": "",
            "authtoken": ""
        },
        navigation: {},
        authstatus: false,
        webToNativeRequest: function (data, action) {
            if (!data) {
                data = defaultParam;
            }
            setupWebViewJavascriptBridge(function (bridge) {
                if (typeof bridge === "undefined") {
                    return;
                }
                try {
                    var param = data;
                    if (data.action != 'authvertify') {
                        param = _extend({}, data, BridgeSDK.authconifg);
                    }
                    bridge.callHandler(action, JSON.stringify(param));
                } catch (ex) {
                    __log__("bridge callHandler exception happened!");
                }
            });
        },
        webToNativeRequestWithCallBack: function (data, action, callBack) {
            if (!data) {
                data = defaultParam;
            }
            setupWebViewJavascriptBridge(function (bridge) {
                if (typeof bridge === "undefined") {
                    __log__("bridge not initialize");
                    return;
                }
                try {
                    var param = data;
                    if (data.action != 'authvertify') {
                        param = _extend({}, data, BridgeSDK.authconifg);
                    }
                    bridge.callHandler(action, JSON.stringify(param), callBack);
                } catch (ex) {
                    __log__("bridge callHandler exception happened!");
                }
            });
        },
        registerHandler: function (action, cb) {
            eventHub.set(action, cb);
        },
        regEvent: function (action, cb) {
            eventHub.set(action, cb);
        },
    };

    // 全局暴露
    // window.BridgeSDK = BridgeSDK;

    /**
     * 授权
     */
    BridgeSDK.config = function (options) {
        var defaults = {
            "appid": "",
            "authtoken": ""
        };
        BridgeSDK.authconifg = _extend(defaults, options);
        var param = {
            'action': 'authvertify',
            'data': BridgeSDK.authconifg
        };
        __log__('进入config函数');
        var that = this;
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            var readyCallBack = callBackList.get("onReady");
            // 授权验证通过
            if (responseData.code === "0") {
                that.authstatus = true;
                readyCallBack && readyCallBack(responseData);
            } else {
                that.authstatus = false;
                readyCallBack && readyCallBack(responseData);
            }
        });
    };

    /**
     * 授权通过后会调用这个接口
     * @param callBack
     */
    BridgeSDK.onReady = function (callBack) {
        if (callBack && typeof callBack === 'function') {
            callBackList.set("onReady", callBack);
        }
    };

    /**
     * 生命周期变化
     * @param callBack
     */
    BridgeSDK.onPageStatusChange = function (callBack) {
        if (callBack && typeof callBack === 'function') {
            // __log__('####当前版本:'+ bridge.version + '#####');
            callBackList.set("onPageStatusChange", callBack);
        }
    };


    /**
     * 开启Api的debug模式，比如出了个什么错误，能alert告诉你
     * @param    {Function}  callback(error) 出错后的回调，默认是alert
     */
    BridgeSDK.enableDebugMode = function (callback) {
        /**
         * @param {String}  errorMessage   错误信息
         * @param {String}  scriptURI      出错的文件
         * @param {Long}    lineNumber     出错代码的行号
         * @param {Long}    columnNumber   出错代码的列号
         */
        window.onerror = function (errorMessage, scriptURI, lineNumber, columnNumber) {
            // 有callback的情况下，将错误信息传递到options.callback中
            if (typeof callback === 'function') {
                callback({
                    message: errorMessage,
                    script: scriptURI,
                    line: lineNumber,
                    column: columnNumber
                });
            } else {
                // 其他情况，都以alert方式直接提示错误信息
                var msgs = [];
                msgs.push("\n错误信息：", errorMessage);
                msgs.push("\n出错文件：", scriptURI);
                msgs.push("\n出错位置：", lineNumber + '行，' + columnNumber + '列');
                alert(msgs.join(''));
            }
        }
    };

    /**
     * 微信APP支付
     * @param options
     * @param callback
     */
    BridgeSDK.webToAppWxpay = function (options, callback) {

        var param = {
            'action': 'wxpay',
            'data': ""
        };
        param = _extend(param, options);
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof callback == 'function') {
                callback(responseData);
            }
        });
    };

    /**
     * 支付宝APP支付
     * @param options
     * @param callback
     */
    BridgeSDK.webToAppAlipay = function (options, callback) {
        let parser = new DOMParser();
        let v = parser.parseFromString(options.data,'text/html');        
        let form = v.querySelector('#alipaysubmit')
        if (form) {
            let fd = new URLSearchParams(new FormData(form)).toString()
            options.data = fd
            let action = form.getAttribute('action')        
            fetch(action+'&'+fd)
            .then((resp) => {
                if (resp.url) {
                    window.location.assign(resp.url)
                }
            })
            .catch((resp) => console.log('err',resp))
            return    
        }
        var param = {
            'action': 'alipay',
            'data': ""
        };
        param = _extend(param, options);        
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {

            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof callback == 'function') {
                callback(responseData);
            }
        });
    };

    /**
     * 埋点事件
     * @param options

     // 埋点请求
     var opts = {
        eventId: "event_customer",
        param: "埋点需要的参数"
     }

     * @param options
     */
    BridgeSDK.event = function (options) {
        var param = {
            'action': 'event',
            'data': "0",
            'eventId': options.eventId,
            'param': {
                "param1": options.param
            }
        };
        console.log("事件统计数据：", param)
        BridgeSDK && BridgeSDK.webToNativeRequest(param, 'nativeHandler');
    };

    /**
     * 通用的跳转到新的页面
     var options = {
        "url" : "http://www.baidu.com",       // 必填,页面地址
        "title": "我是标题",                   // 可选,自定义标题,不传的话取网页的title
        "type" : "",                          // 原生界面标志
        "pageTag": "PageCustomerTag",         // 可选,页面唯一标志，方便运营统计分析
        "navigation": {
            "isHideNavBar": false,            // 是否隐藏导航栏，默认false
            "isHideNavBarDividerLine": false, // 是否隐藏导航栏下的分割线，默认false
            "title": "标题",                   // 标题, 优先级最高
            "titleColor": "255,255,255",      // 标题颜色,默认白色
            "titleFontSize": "20",            // 标题字体大小
            "navbarBackgroundColor": "0,0,0", // 导航栏背景颜色
            "rightButtonType": "2",           // 1显示刷新(默认) 2自定义按钮
            "rightButtonTitle": "按钮标题",    // 按钮标题
            "rightButtonTitleColor": "255,255,255", // 标题文字颜色
            "rightButtonAction": "someFuncName", // 按钮触发的js事件名称
        }
     }
     * @param options
     */
    BridgeSDK.goToWebPage = function (options) {
        var param = {
            'action': 'push_web',
            'data': options.url,
            'type': options.type ? options.type : "",
            'tag': options.pageTag ? options.pageTag : "",
            'ext': options.navigation ? JSON.stringify(options.navigation) : JSON.stringify({}),
        };

        // 是否需要页面回调数据

        if (options.onPageCallBack && typeof options.onPageCallBack === 'function') {
            callBackList.set('onPageCallBack', options.onPageCallBack);
            param.enablePageCallBack = "1";
        } else {
            param.enablePageCallBack = "0";
        }

        BridgeSDK && BridgeSDK.webToNativeRequest(param, 'nativeHandler');
    };

    /**
     * 关闭页面

     var options = {
        target: "", // target为""，关闭当前页面；root:回退到导航跟视图
     }
     * @param option
     */
    BridgeSDK.closePage = function (options) {

        console.log(options);

        var param = {
            'action': 'pop_web',
            'data': (options && options.data) || "",
            'target': (options && options.target) || ""
        };
        BridgeSDK && BridgeSDK.webToNativeRequest(param, 'nativeHandler');
    };

    /**
     * 获取App的版本
     * @param callback
     */
    BridgeSDK.getAppVersion = function (callback) {
        var param = {
            'action': 'version',
            'data': {}
        };

        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof callback == 'function') {
                callback(responseData);
            }
        });
    };

    // Token失效，退出到登录界面
    BridgeSDK.invalidUserToken = function (options) {
        var param = {
            'action': 'invalid_token',
            'data': options
        };
        BridgeSDK && BridgeSDK.webToNativeRequest(param, 'nativeHandler');
    };

    /**
     * 1.5.3.24版本开始支持
     * 支持签名
     * @param options
     *
     * data必须是字符串
     * sign必须是字符串"0"或者"1"
     * cb回调结果, code=0表示成功，非0表示失败
     *
     * 使用实例eg:

     BridgeSDK.encrypt({
        data: JSON.stringify({a:'a', b:'b'}),
        sign: '1',
        cb:  function(res) {
            if (res.code == 0) {
                console.log("加密结果:" + res.data)
            } else {
                console.log("加密失败:" + res.msg)
            }
        }
    })

     */

    BridgeSDK.encrypt = function (options) {

        var param = {
            'action': 'encrypt',
            'data': options.data,
            'sign': options.sign
        };

        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };


    /**
     * 更新UI操作，1.5.3.24版本开始支持
     *

     options说明:

     var options = {
        disable_bounds: "1",
        hide_nav_divider: "0",
        enablePullToRefresh："1"
     }

     *
     * @param options
     */
    BridgeSDK.updateUI = function (options) {
        var param = {
            'action': 'ui',
            'data': options
        };
        BridgeSDK && BridgeSDK.webToNativeRequest(param, 'nativeHandler');
    };

    /**
     * 1.6.1版本
     *
     如果web页面使用自己的路由，需要在每个路由界面都重新设置按钮，否则公用同一个按钮
     示例:
     BridgeSDK.setupNavigation({
        title: '奥特曼',
        titleColor: '255,255,0',
        titleFontSize: '20',
        navbarBackgroundColor: '255,0,0',
        isHideNavBarDividerLine: "1",
        rightButtonType: "2",
        rightButtonTitle: '哈哈',
        rightButtonTitleColor: '255,255,255',
        rightButtonTitleFontSize: "16",
        onClickRightButton: function (data) {
            alert("哈哈右侧按钮被点击")

        }
    })

     *
     * 设置导航栏
     * @param options
     */
    BridgeSDK.setupNavigation = function (options) {

        if (options.rightButtonType && options.rightButtonType == 2) {
            if (options.onClickRightButton && typeof options.onClickRightButton === 'function') {
                callBackList.set('onRightBtnClick', options.onClickRightButton)
            }
        }

        var param = {
            'action': 'navigation',
            'data': options
        };
        BridgeSDK && BridgeSDK.webToNativeRequest(param, 'nativeHandler');
    };

    /**
     * 授权登录

     options:对象
     {
        cb: function(data) {}
     }

     * @param options
     */
    BridgeSDK.authLogin = function(options) {
        var param = {
            'action': 'authLogin',
            'data': options.data || ''
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };

    /**
     * 上传图片
     * @param options
     */
    BridgeSDK.uploadImage = function(options) {
        var param = {
            'action': 'upload_image',
            'data': options.data || {}
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };

    /**
     * 保存图片到本地
     * @param options
     */
    BridgeSDK.saveImage = function(options) {
        var param = {
            'action': 'save_image',
            'data': options.data || ''
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };

    /**
     * 打开百川页面
     * options.data = url
     * @param options
     */
    BridgeSDK.openBaiChuanPage = function(options) {
        var param = {
            'action': 'baichuan',
            'data': options.data || ''
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };

    /**
     * 淘宝客授权
     * var options = {
     *     data: "http://oatuh.taobaoke.com/xxxxxxx",
     *     cb: function(data) {
     *         console.log(data);
     *     }
     * }
     * BridgeSDK.taobaokeAuth(options);
     * @param options
     */
    BridgeSDK.taobaokeAuth = function(options) {
        var param = {
            'action': 'taobaoke',
            'data': options.data || ''
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };


    /**
     * var options = {
     *     data: '需要复制的内容',
     *     cb: function(data) {
     *
     *     }
     * }
     *
     * 复制操作
     * @param options
     */
    BridgeSDK.copy = function (options) {
        var param = {
            'action': 'copy',
            'data': options.data || ''
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };

    BridgeSDK.paste = function (options) {
        var param = {
            'action': 'paste',
            'data': options.data || ''
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };


    /**
     * 分享
     *
     * var options = {
     *     data: {
     *         platform: "暂时只支持：0表示微信好友，1朋友圈",
     *         type: "0:文本 1:图片 2:网页"
     *         title: "分享的标题",
     *         text: "文本内容，例如淘口令",
     *         url: "跳转的url",
     *         image: "http://xxxxxx/xxxx.jpg"
     *     },
     *     cb: (result) => {
     *         console.log(result);
     *     }
     * }
     *
     * BridgeSDK.share(options)
     *
     * @param options
     */
    BridgeSDK.share = function (options) {
        var param = {
            'action': 'share',
            'data': options.data || {}
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };

    /**
     * 微信登陆
     *
     * @param options
     */
    BridgeSDK.wechatLogin = function (options) {
        var param = {
            'action': 'wechat_auth',
            'data': options.data || ''
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    };

    /***
     * 获取用户登录信息
     *
     * @param method
     * @param data
     * @param cb
     */
    BridgeSDK.userInfo = function (options) {

        var param = {
            'action': 'user_info',
            'data': options.data || ''
        };

        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof options.cb == 'function') {
                options.cb(responseData);
            }
        });
    }

    /**
     * 公用的调用方法
     *
     * @param method 方法签名
     * @param data 传递的数据
     * @param cb 回调结果
     */
    BridgeSDK.call = function (method, data, cb) {
        var param = {
            'action': method,
            'data': data || ''
        };
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {
            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof cb == 'function') {
                cb(responseData);
            }
        });
    };

    /**
     * WECHAT
     * @param options
     * @param callback
     */
    BridgeSDK.webToAppWechatPay = function (options, callback) {
        let parser = new DOMParser();
        console.log(options);
        var param = {
            'action': 'wechat',
            'data': ""
        };
        param = _extend(param, options);
        BridgeSDK && BridgeSDK.webToNativeRequestWithCallBack(param, "nativeHandler", function (responseData) {

            if (typeof (responseData) == "string") {
                responseData = JSON.parse(responseData);
            }
            if (typeof callback == 'function') {
                callback(responseData);
            }
        });
    };
    BridgeSDK.iosOnly = function() {
        if (isiOS) {
            return true
        }
        return false
    }

    BridgeSDK.androidOnly = function() {
        if (isAndroid) {
            return true
        }
        return false
    }

    return BridgeSDK;
});
