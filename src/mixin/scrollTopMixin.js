export default {
    data() {
        return {
            contentBoxTop: 0,

        }
    },

    beforeRouteLeave(to, form, next) {
        this.contentBoxTop = this.$refs.mainBox.scrollTop
        next()
    },

    activated() {
        this.$refs.mainBox.scrollTop = this.contentBoxTop
        this.init()

    },
}