import moment from 'moment';
export default {
    filters: {


        toP(v){
            return v*100 +'%'
        },

        //数字加减号 
        scoreType(v) {
            if (v == 0) {
                return '+'
            } else {
                return '-'
            }
        },



        isNaN0(val) {
            let v = parseFloat(val)
            if (isNaN(v)) { v = 0 }
            return v
        },

        toNum(v,n){
            let v1 = parseFloat(v)
            if (isNaN(v1)) { 
                return v
             }
            return v1.toFixed(n)
          
        },

        money(v){
            return '¥ '+v
        },
        

        orderStatusClass(v){
            if(v==1){return 'm_blue'}
            if(v==3){return 'mm_green'}
            if(v==4){return 'mm_red'}
        },


  

        //时间转换
        times(v) {
            let data = moment(v).format('YYYY-MM-DD')
            let nowData = moment().format('YYYY-MM-DD') //今天
            let yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD') //昨天
            if (data == nowData) {
                if (moment(v).format('HH:mm') == moment().format('HH:mm')) {
                    return '刚刚'
                }
                return moment(v).format('HH:mm')
            } else if (data == yesterday) {
                return '昨天 ' + moment(v).format('HH:mm')
            } else {
                return data
            }


        },

        BaseData(v, module, key_name) {
            let str_baseData = sessionStorage['baseData'];
            let baseData = str_baseData && JSON.parse(str_baseData) || [];
            let arr = baseData.filter(i => i.module == module && i.key_name == key_name);
            let item = arr.find(i => i.id == v);
            if (item) {
                return item.name;
            } else {
                return v;
            }
        },

        avatar(v){
            if(!v){
              return  require('../../assets/img/no_pic.png')
            }
            return v
          },

          unit(v){
              if(!v){
                  return '件'
              }
              return v
          },

          replaceStr(v,s2="/"){
              v = v + ''
              return v.replace(/\_/g,'/')

          }



    }
}