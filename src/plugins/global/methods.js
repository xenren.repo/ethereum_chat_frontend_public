import store from '@/store'
import areaList from '@/utils/area.js'
export default {
  methods: {
    //获取省份
    get_province(v) {
      let List = areaList.province_list
      return List[v]
    },
    //获取市
    get_city(v) {
      let List = areaList.city_list
      return List[v]
    },
    //改变标题
    sePageTitle(title) {
      store.dispatch('setPageTitle', title)
    },
    //改变是否显示 0 为false 1为true
    sePageTitleShow(v) {
      store.dispatch('setPageTitleShow', v)
    },

    //是否返回键状态  0 为false 1为true
    setIsBack(v) {
      store.dispatch('setIsBack', v)
    },

    //深拷贝方法
    _dep(obj) {
      return JSON.parse(JSON.stringify(obj))
    },

    //后退
    back() {
      this.$router.go(-1)
    },

    refashView() {
      try {
        plus.runtime.restart()
      } catch (error) {
        window.location.reload()
      }

    },



  }
}