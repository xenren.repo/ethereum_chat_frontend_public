import axios from 'axios'
import _ from 'lodash'
import router from '../router/index'
import store from '@/store'
import config from '@/config'
import * as sdk  from '@/utils/sdk'
import {
    Toast,Dialog
} from 'vant';
const requests = []
let baseURL = config[process.env.VUE_APP_ENV].host

export default (Vue) => {
    Object.defineProperties(Vue.prototype, {
        $http: {
            value: axios
        }
    })
    //axios配置
    axios.defaults.baseURL = baseURL

    axios.defaults.timeout = 30000
    axios.defaults.withCredentials = true


    axios.defaults.headers['Content-Type'] = 'application/json'
    // 添加拦截器
    axios.interceptors.request.use(function (config) {
        store.dispatch('setLoading', true)
        // console.log(config)   
        let token = sessionStorage.token || localStorage.token || ''

        config.headers.Authorization = `Bearer ${token}`


        return config
    }, function (error) {
        store.dispatch('setLoading', false)
        return Promise.reject(error)
    })

    axios.interceptors.response.use(function (response) {
        // console.log(response)
        _.remove(requests, r => {
            return r === response.config
        })

        if (!requests.length) {

            setTimeout(() => {
                store.dispatch('setLoading', false)
            }, 100)
        }

        //未登录处理
        if (response.data.code != 0) {
            Toast.clear()
           
    
            if (response.data.code == '1000'||response.data.code == '1100') {
                localStorage.token = '' 
                sessionStorage.token=''
 
                sdk.invalidUserToken()
            }else{
                setTimeout(() => {
                    Toast(response.data.message)
                }, 300)
            }

           



            return Promise.reject(response.data)
        }

        return response.data
    }, function (error) {
        Toast.clear()
        store.dispatch('setLoading', false)

        if (error.response) {
            //console.log(error.response.status)
            switch (error.response.status) {
                case 401:
                    console.log('401 未授权')
                    break
                case 500:
                    console.log('500')
                    break
                default:
                    console.log('发生错误了')
            }
        } else {

        }
        return Promise.reject(error)
    })
}