import intercepter from './intercepter'
import globalFilters from './global/filters'
import globalMethods from './global/methods'
import {goToWebPage} from '@/utils/sdk'

import { Locale } from 'vant';
import zhCN from 'vant/lib/locale/lang/zh-CN';
import zhHK from 'vant/lib/locale/lang/zh-HK';
import config from '@/config'
import { Toast, Dialog } from 'vant';
import * as api from '../api/index'
import nodata from '@/components/noData.vue'
import sel_mixin from './sel_mixin' //独自的mixin
import store from '@/store'
import * as utils from '../utils'
import zwAddress from '@/components/zwAddress'



const ENV = process.env.VUE_APP_ENV
export default {
    install(Vue, opts) {
        intercepter(Vue, opts)
        Vue.component('nodata', nodata),
        Vue.component('zwAddress', zwAddress)
        Vue.mixin({
            mixins: [globalFilters, globalMethods, sel_mixin],
       
            data() {
                return {
                    m_utils: utils,
                    api,
                    sit_info: {
                        coin_name: ''
                    },
                    m_env: ENV,
                    m_color: '#4C80C1',
                    formData: {},
                    _formData: {},

                    _m_pageInfo: {},
                    m_pageInfo: {
                        limit: 10,
                        page: 1,
                    },
                    m_data: [],
                    list_loading: true,
                    list_finished: false,
                    list_fresh: false,
                    m_loadList_time: null,
                    oTop:0


                }
            },
            filters: {

            },
            computed: {
                //滚动加载没有数据
                m_hasData() {
                    return !!this.m_data.length
                },

                userinfo() { return store.state.userInfo || {} },

                isLogin() {
                    let userInfo = store.state.userInfo || {}

                    return userInfo.id
                }

            },
            mounted() {
                this.initMounted()
            },

            methods: {
                initMounted() {
                    this._formData = this._dep(this.formData)
                    this._m_pageInfo = this._dep(this.m_pageInfo)
                    let _imgUrl = '/images/upload'
                    let _customUploader = '/user/upload-file'
                    if (this.m_env != 'sit') {

                        this.m_action = config[process.env.NODE_ENV].host + _imgUrl
                        this.m_custom_uploader = config[process.env.NODE_ENV].host + _customUploader
                        this.m_host = config[process.env.NODE_ENV].host
                    } else {
                        this.m_action = window.location.origin + _imgUrl
                        this.m_custom_uploader = window.location.origin + _customUploader
                    }
                },

                m_resetFormData() {
                    this.formData = this._dep(this._formData)
                },
                m_toast(msg) {
                    Toast(msg)
                },
                m_success(msg) {
                    Toast.success(msg)
                },
                m_loading(message) {
                    Toast.loading({
                        message,
                        mask: true,
                        duration: 0
                    })
                },
                m_hideLoading() {
                    Toast.clear()
                },
                //滚动加载
                m_loadList() {
                    this.m_loadList_time&&clearTimeout(this.m_loadList_time)
                    this.m_loadList_time = setTimeout(() => {
                        this.getDataList().then(res => {
                            this.list_loading = false;
                            this.list_fresh = false
                            if (res.data.total <= this.m_data.length || this.m_pageInfo.page >= res.data.last_page) {
                                /* 所有数据加载完毕 */
                                // console.log(111,res.data.total,this.m_data.length,this.m_pageInfo.page,res.data.last_page)
                                this.list_finished = true;
                                return
                            }
                            /* 单次请求数据完毕 */

                            this.list_finished = false
                            this.m_pageInfo.page++
                        }).catch(err => {
                            this.list_loading = false
                            this.list_finished = true;

                        })

                    }, 100);

                },

                //初始化 滚动加载数据
                clearScrollResult() {
                    this.m_data = []
                    this.m_pageInfo = this._dep(this._m_pageInfo)
                    this.list_loading = false
                    this.list_finished = true


                },

                //获取用户信息
                getUserinfo() {
                    let token = sessionStorage.token
                    if (!token) { return Promise.resolve() }
                    return this.api.getUserInfo().then(res => {
                        store.dispatch('setUserInfo', res.data)
                        //sessionStorage.userInfo = JSON.stringify(this.userinfo)
                        return res
                    })
                },


                isNaN0(val) {
                    let v = parseFloat(val)
                    if (isNaN(v)) { v = 0 }
                    return v
                },

                //获取公共数据
                getBaseData(obj) {
                    let baseData = sessionStorage.baseData && JSON.parse(sessionStorage.baseData) || []

                    let arr = baseData.filter(i => i.module == obj.module && i.key_name == obj.key_name)
                    return arr
                },

                GetBaseData(module, key_name) {
                    let baseData = sessionStorage.baseData && JSON.parse(sessionStorage.baseData) || []

                    let arr = baseData.filter(i => i.module == module && i.key_name == key_name)
                    return arr

                },




                get_sit_info() {
                    return this.api.site_info().then(res => {
                        sessionStorage.site_info = JSON.stringify(res.data)
                        this.sit_info = res.data
                        return res

                    })
                },





                coverEditData(formData, editData) {
                    let _formData = this._dep(formData)
                    let _editData = this._dep(editData)
                    let _formDataKey = Object.keys(_formData)
                    _formDataKey.forEach(i => {
                        _formData[i] = _editData[i]
                    })
                    return _formData
                },

                //改变语言  1、简体  2繁体
                changeLang(v) {
                    if (v == 1) {
                        this.$i18n.locale = "zh";
                        Locale.use('zh_CN', zhCN);
                    }
                    if (v == 2) {
                        this.$i18n.locale = "tw";
                        Locale.use('zh-HK', zhHK);
                    }
                },

                //改变语言 重新拉去公共数据
                data_dictionary() {

                    api.data_dictionary().then(res => {
                        sessionStorage.baseData = JSON.stringify(res.data)
                        api.site_info().then(res => {
                            sessionStorage.site_info = JSON.stringify(res.data)
                        })
                    })
                },

                m_resetData() {
                    Object.assign(this.$data, this.$options.data())
                    this.initMounted()
                },

                oneFloorArr(arr) {
                    return [].concat(...arr.map(i => Array.isArray(i) ? this.oneFloorArr(i) : i))
                },

                nowCoding() {
                    Dialog.alert({
                        title: '提示',
                        message: '尚未开放，敬请期待！'
                    }).then(() => {
                        // on close
                    });

                },

                //获取sessionStorage 里的值
                getSSG(key, def = {}) {

                    return sessionStorage[key] && JSON.parse(sessionStorage[key]) || def
                },

                openUrl(url){
                  let e = new RegExp(/^(?:(?:https?|ftp):\/\/)?(?:[\da-z.-]+)\.(?:[a-z.]{2,6})(?:\/\w\.-]*)*\/?/)
                  let isurl = e.test(url)
                  if(isurl){
                    goToWebPage(url)
                  }else{
                      this.$router.push(url)
                  }
                },

                

            }
        })
    }

}