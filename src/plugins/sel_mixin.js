import { param2Obj } from '@/utils/index'
export default {
    methods: {
        //验证登录
        veriLogin() {

            let token = sessionStorage.token || ''
            if (!token) {
                this.$dialog.alert({
                    title: '提示',
                    message: '还未登录，请先登录！'
                }).then(() => {
                    this.$router.go(-1)
                })
                return false
            }
            return true
        },


 


    },
    filters:{
        img(v,size="310x310"){
            if(v.indexOf('img.alicdn.com')!=-1){
                return `${v}_${size}.jpg`
            }
            return v
        }
    }
}