import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

let state = {
    loading: false,
    pageTitile:sessionStorage.pageTitile || '',
    pageTitileShow:sessionStorage.pageTitileShow  ||'1' ,//1 为展示 0为不展示
    isBack:sessionStorage.isBack || '1' ,  //1 为展示 0为不展示  后退键
    userInfo:sessionStorage.userInfo && JSON.parse(sessionStorage.userInfo)  || {} ,
    titleBtn1:sessionStorage.titleBtn1 || '0', //添加代理展示

}

let actions = {
    setLoading({ commit }, info) {
        commit('SET_LOADING', info)
    },
  
    setPageTitle({ commit }, info) {
        commit('SET_PAGE_TITLE', info)
    },

    setPageTitleShow({ commit }, info) {
        commit('SET_PAGE_TITLE_SHOW', info)
    },

    setIsBack({ commit }, info){
        commit('SET_IS_BACK', info)
    },

    setTitleBtn1({ commit }, info){
        commit('SET_TitleBtn1', info)
    },

    setUserInfo({ commit }, info){
        commit('SET_USER_INFO', info)

    },

}

let mutations = {
    SET_LOADING(state, info) {
        state.loading = info
    },

    SET_PAGE_TITLE(state, info){
        document.title = info
        sessionStorage.pageTitile = info
        state.pageTitile = info
    },

    SET_PAGE_TITLE_SHOW(state, info){
      
        sessionStorage.pageTitileShow = info
        state.pageTitileShow = info
    },

    SET_IS_BACK(state, info){
        sessionStorage.isBack = info
        state.isBack = info
    },

    SET_TitleBtn1(state, info){
        sessionStorage.titleBtn1 = info
        state.titleBtn1 = info
    },

    SET_USER_INFO(state, info){
        sessionStorage.userInfo = JSON.stringify(info)
        state.userInfo = info
    },

}

let getters = {
    loading: state => state.loading,
    pageTitile: state => state.pageTitile,
    pageTitileShow: state => state.pageTitileShow,
    isBack: state => state.isBack,
    titleBtn1: state => state.titleBtn1,
    userInfo: state => state.userInfo
}


export default  new Vuex.Store({
    state,
    actions,
    mutations,
    getters,
    
   
})

