import router from './'
import store from '@/store'
import { initData, handelLogin } from '../utils/loginInitData'
import { angent, judgeOption } from '../utils/judgeUserAgent'
import * as api from '@/api'
const hideLoding = () =>{  document.getElementById('loading').style.display = 'none'}

const getBaseInfo = ()=>{
    if( sessionStorage.baseData){
        return Promise.resolve()
    }
    return api.data_dictionary().then(res=>{
        sessionStorage.baseData = JSON.stringify(res.data)
        return res
    })
}

const getSizeInfo = ()=>{
    if( sessionStorage.site_info){
        return Promise.resolve()
    }
    return api.site_info().then(res=>{
        sessionStorage.site_info = JSON.stringify(res.data)
        return res
    })
}

const getToken = () =>{
    if (process.env.VUE_APP_ENV == 'development' || location.href.includes('root')) {
       return Promise.resolve()
    }
    let token = sessionStorage.token || ''
    if( token){
        return Promise.resolve(token)
    }
    return new Promise((resolve,reject)=>{
        BridgeSDK.userInfo({
            cb(res){
                let data = res.data
                if(res.errno!=0){
                    reject(res)
                    return
                }
                let _token = data.token
                sessionStorage.token = _token
                resolve(_token)
            }
        })
    })
}


//路由拦截
router.beforeEach(async (to, from, next) => {
    sessionStorage.isApp = 1

    store.dispatch('setPageTitle', to.meta.title)
    store.dispatch('setIsBack', to.meta.isBack || '1')
    store.dispatch('setPageTitleShow', to.meta.pageTitileShow || '0')
    await getBaseInfo()
    await getSizeInfo()
    await getToken()


    hideLoding()

    if (window.location.href.includes('isApp=1')) {
        // store.dispatch('setPageTitleShow', 0)
        let isBaseData = !!sessionStorage.baseData
        if (!isBaseData) {
            await initData()
        }
        if (!sessionStorage.token) {
            handelLogin().then(res => {
                sessionStorage.token = res.data.token;
                sessionStorage.id = res.data.info.id;
                sessionStorage.expire = res.data.expire + '000'  //PHP时间戳 不给毫秒的 自己加3个0
                hideLoding()
                next()
            }).catch(err => {
                hideLoding()
                next()
            })

        }else{
            hideLoding()
            next()
        }


    } else {

        next()

        // hideLoding()
        // //非app处理
        // let ANGENT = sessionStorage.ANGENT
        // if (!ANGENT) {
        //     ANGENT = await angent()
        //     sessionStorage.ANGENT = ANGENT
        // }
        // if (ANGENT != 'web') {
        //     store.dispatch('setPageTitleShow', 0)
        // }


        // if (to.path == '/login') {
        //     console.log(to)
        //     if (to.query.go || sessionStorage.isGo == 1) {
        //         next()
        //     } else {
        //         judgeOption(to, from, next)
        //     }
        // } else {
        //     next()
        // }

    }












})

router.onError((error) => {
    const pattern = /chunk (.)+ failed/g;
    const isChunkLoadFailed = error.message.match(pattern);
    const targetPath = router.history.pending.fullPath;
    if (isChunkLoadFailed) {
        router.replace(targetPath);
    }
});
