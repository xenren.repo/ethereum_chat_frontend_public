import Vue from 'vue'
import Router from 'vue-router'
import _import from './_import'
import {getBaseUrl } from '@/utils/index'
import {goToWebPage} from '@/utils/sdk'
const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
     let flag = window.location.href.includes('isAppHome=1')
     if(flag){
         let url = getBaseUrl() + location
        // window.location.href = url
        goToWebPage(url)
         return
     }
    return routerPush.call(this, location).catch(error=> error)
  
}




Vue.use(Router)

export default new Router({
    // mode: 'history',  //去掉url中的#
    routes: [{
        path: '/',
        redirect: '/index'
    },
    {
        path: '/index',
        name: 'index',
        meta: { title: '主页' },
        component: _import('index'),
        redirect: '/index/home',
        children: [
            { path: '/index/home', name: 'home', meta: { pageTitileShow: '0', title: '主页', action: 0, isBack: '0', out: 1, i18n: 'index' }, component: _import('home/index') },
            { path: '/index/mall', name: 'mall', meta: { title: '商场', action: 1, isBack: '0', i18n: 'index_mall' }, component: _import('mall/index') },
            { path: '/index/car', name: 'car', meta: { title: '购物车', action: 2, isBack: '0', i18n: 'car' }, component: _import('car/index') },
            { path: '/index/my', name: 'my', meta: { title: '我的', action: 3, isBack: '0', i18n: 'index_my' }, component: _import('my/index') },
            { path: '/index/pinpai', name: 'my_pinpai', meta: { title: '品牌券明细', action: 4, isBack: '0', i18n: 'pinpai' }, component: _import('my/pinpai') },
            { path: '/index/pinpai/exchange', name: 'pinpaiExchange', meta: { title: '商城券转换', action: 5, isBack: '0', i18n: 'pinpaiExchange' }, component: _import('my/pinpaiExchange') },
            { path: '/index/upload', name: 'uploadFiles', meta: { title: '商城券转换', action: 6, isBack: '0', i18n: 'pinpaiExchange' }, component: _import('my/uploadFiles') },
            { path: '/index/credit4', name: 'credit4', meta: { title: '牡丹专区券', action: 7, isBack: '0', i18n: 'pinpai' }, component: _import('my/credit4') },
        ]
    },
    {
        path: '/login',
        name: 'login',
        meta: { title: '登录', pageTitileShow: '0', out: 1, i18n: 'login' },
        component: _import('login')
    },

    {
        path: '/register',
        name: 'register',
        meta: { title: '注册', i18n: 'register' },
        component: _import('register')
    },

    {
        path: '/findPwd',
        name: 'findPwd',
        meta: { title: '找回密码', i18n: 'findPwd' },
        component: _import('findPwd')
    },

    {
        path: '/updataPwd',
        name: 'updataPwd',
        meta: { title: '修改密码', i18n: 'updataPwd' },
        component: _import('updataPwd')
    },


    {
        path: '/goodsList',
        name: 'goodsList',
        //商品列表
        meta: { title: '商品', i18n: 'goodsList' },
        component: _import('mall/goodsList')
    },



    {
        path: '/goodsDetail',
        name: 'goodsDetail',
        meta: { title: '商品详情', i18n: 'goodsDetail' },
        component: _import('mall/goodsDetail')
    },

     {
        path: '/reportSeller',
        name: 'reportSeller',
        //商品列表
        meta: { title: '商品', i18n: 'goodsList' },
        component: _import('report/index')
    },



    {
        path: '/userInfo',
        name: 'userInfo',
        meta: { title: '个人信息', i18n: 'userInfo' },
        component: _import('my/userInfo')
    },


    {
        path: '/address',
        name: 'address',
        meta: { title: '收货地址', i18n: 'address' },
        component: _import('menu/address')
    },

    {
        path: '/receipt',
        name: 'receipt',
        meta: { title: '收货地址', i18n: 'address' },
        component: _import('menu/receipt')
    },

    {
        path: '/addAddress',
        name: 'addAddress',
        meta: { title: '收货地址', i18n: 'addAddress' },
        component: _import('menu/addAddress')
    },

    {
        path: '/suerOrder',
        name: 'suerOrder',
        meta: { title: '确定订单', i18n: 'suerOrder' },
        component: _import('mall/suerOrder')
    },

    {
        path: '/goodsPay',
        name: 'goodsPay',
        meta: { title: '收银台', i18n: 'goodsPay' },
        component: _import('mall/goodsPay')
    },

    {
        path: '/order',
        name: 'order',
        meta: { title: '订单列表', i18n: 'order' },
        component: _import('order/index')
    },

    {
        path: '/orderDetial',
        name: 'orderDetial',
        meta: { title: '订单详情', i18n: 'orderDetial' },
        component: _import('order/orderDetial')
    },

    {
        path: '/package_detail',
        name: 'package_detail',
        meta: { title: '物流信息', i18n: 'package_detail' },
        component: _import('order/package_detail')
    },

    {
        path: '/evaluate',
        name: 'evaluate',
        meta: { title: '商品评价', i18n: 'evaluate' },
        component: _import('order/evaluate')
    },

    {
        path: '/refund',
        name: 'refund',
        meta: { title: '退款申请', i18n: 'refund' },
        component: _import('order/refund')
    },

    {
        path: '/refundList',
        name: 'refundList',
        meta: { title: '退换货', i18n: 'refundList' },
        component: _import('order/refundList')
    },
    {
        path: '/refundiDetail',
        name: 'refundiDetail',
        meta: { title: '售后申请', i18n: 'refundiDetail' },
        component: _import('order/refundiDetail')
    },

  
    {
        path: '/sys_info_details',
        name: 'sys_info_details',
        meta: { title: '系统公告', i18n: 'sys_info' },
        component: _import('menu/sys_info_details')
    },

    {
        path: '/sys_info',
        name: 'sys_info',
        meta: { title: '系统公告', i18n: 'sys_info' },
        component: _import('menu/sys_info')
    },

    {
        path: '/balance',
        name: 'balance',
        meta: { title: '余额明细', i18n: 'balance' },
        component: _import('menu/balance')
    },

    {
        path: '/voucherList',
        name: 'voucherList',
        meta: { title: '专区券明细', i18n: 'voucherList' },
        component: _import('menu/voucher/voucherList')
    },

    {
        path: '/vpanList',
        name: 'vpanList',
        meta: { title: '品牌券明细', i18n: 'vpanList' },
        component: _import('menu/voucher/vpanList')
    },

    {
        path: '/recharge',
        name: 'recharge',
        meta: { title: '专区券充值', i18n: 'recharge' },
        component: _import('menu/voucher/recharge')
    },

    {
        path: '/rechargeList',
        name: 'rechargeList',
        meta: { title: '专区券充值记录', i18n: 'rechargeList' },
        component: _import('menu/voucher/rechargeList')
    },
    // {
    //     path: '/about',
    //     name: 'about',
    //     meta: { title: '关于我们', i18n: 'about' },
    //     component: _import('menu/about')
    // },
    {
        path: '/about_detail',
        name: 'about_detail',
        meta: { title: '关于我们', i18n: 'about_detail' },
        component: _import('menu/about_detail')
    },

    {
        path: '/qrcode',
        name: 'qrcode',
        meta: { title: '邀请好友', i18n: 'qrcode' },
        component: _import('menu/qrcode')
    },

    {
        path: '/upload',
        name: 'upload',
        meta: { title: 'UPLOAD', i18n: 'qrcode' },
        component: _import('menu/my/uploadFiles')
    },

    {
        path: '/about',
        name: 'about',
        meta: { title: '关于我们', i18n: 'about' },
        component: _import('about/index')
    },
    {
        path: '/about_us',
        name: 'about_us',
        meta: { title: '公司简介', i18n: 'about_us' },
        component: _import('about/aboutOld')
    },
    {
        path: '/about_email',
        name: 'about_email',
        meta: { title: '联系我们', i18n: 'contact' },
        component: _import('about/aboutEmail')
    },

     


        // {
        //     path: '/setLang',
        //     name: 'setLang',
        //     meta: { title: '设置语言', i18n: 'setLang' },
        //     component: _import('menu/setLang')
        // },












    ]
})