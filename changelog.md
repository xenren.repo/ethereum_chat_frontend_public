# Changelog

All notable changes to this project will be documented in this file.

## [Merged]

## 2020-07-09

### Added

- Added `iosOnly()` in `BridgeSDK` : it will be needed when iOS need special treatment
- Added `androidOnly()` in `BridgeSDK` : it will be needed when android need special treatment