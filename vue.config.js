const EVN = process.env.VUE_APP_ENV
const isProuction = EVN == 'sit'
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
  outputDir: process.env.outputDir,
  assetsDir: 'static',
  indexPath:'mobile.html',
  publicPath: './',
  productionSourceMap: false,
  transpileDependencies:['thread-loader'],

  devServer: {
    port: 8080,
    open:true,
    // proxy: {
    //   '/api': {
    //     target: 'https://qqdev.1688qia.com',
    //     changeOrigin: true,
    //     pathRewrite: {
    //       "^/api": "/"
    //     }
    //   }
    // }
  },
  configureWebpack: config => {
    config.performance = {
      hints: false
    }
    if (isProuction) {

      // config.plugins.push(
      //   new CompressionWebpackPlugin({
      //     algorithm: 'gzip',
      //     test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
      //     threshold: 10240,
      //     minRatio: 0.8,
      //    //  deleteOriginalAssets: true,//删除原文件
      //   })
      // )
      // config.plugins.push(
      //   new UglifyJsPlugin({
      //     uglifyOptions: {
      //       warnings: false,
      //       compress: {
      //         drop_debugger: true,
      //         drop_console: true  //去除 console
      //       }
      //     },
      //     sourceMap: true,
      //     parallel: true
      //   })
      // )
    }

  },
}